# IHiBO 2.0 - Intelligent Human-input-based Blockchain Oracle 2.0
This project aims to develop version 2.0 of IHiBO. This will be done first and foremost by adding probabilistic reasoning.

# IHiBO - Intelligent Human-input-based Blockchain Oracle
A cross-chain oracle that enables the execution and traceability of formal argumentation and negotiation processes, involving the intervention of human experts
